class nrpe {
  exec { 'apt-update':
    command => '/usr/bin/apt-get update',
  }
  package { "nagios-nrpe-server":
    require => Exec['apt-update'],
    ensure	=> installed,
  }

  file  { "/etc/nagios/nrpe.cfg":
    source 	=> "puppet:///modules/nrpe/nrpe.cfg",
    owner  	=> root,
    group	=> root,
    mode 	=> 0644,
    notify 	=> Service['nagios-nrpe-server'],
    require => Package['nagios-nrpe-server'],
  }

  service { 'nagios-nrpe-server':
    ensure => 'running',
    enable => true,
  }
}
