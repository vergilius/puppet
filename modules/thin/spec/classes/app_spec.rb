require 'spec_helper'

shared_examples_for "all thin::app configurations" do
  it { should compile.with_all_deps }
end

describe 'thin::app' do
  describe "with basic parameters" do
    let(:params) {{
      :chdir          => '/tmp/demoapp',
      :user           => 'nobody',
      :group          => 'nobody',
      :rackup         => '/tmp/demoapp/config.ru',
      :name           => 'demoapp',
    }}
    context 'Debian' do
      let(:facts) {{
        :operatingsystem        => 'debian',
        :operatingsystemrelease => '7',
        :osfamily               => 'debian',
        :puppetversion          => '3.4.2',
        :concat_basedir         => '/foo',
        :kernel                 => 'linux',
        :lsbdistid              => 'debian',
        :lsbdistcodename        => 'wheezy',
      }}
      it_behaves_like "all thin::app configurations"

      it { should contain_package('thin') }

      it do
        should contain_service('thin-demoapp').with({
          :ensure => "running"
        })
      end
    end
  end
end
