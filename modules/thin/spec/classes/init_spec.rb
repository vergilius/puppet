require 'spec_helper'

shared_examples_for "all thin configurations" do
  it { should compile.with_all_deps }
end

describe 'thin' do
  describe "with no parameters" do
    context 'Debian-derived distros' do
      ['Debian','Ubuntu'].each do |operatingsystem|
        context "#{operatingsystem}" do
          let(:facts) {{
            :operatingsystem => operatingsystem,
            :osfamily        => 'debian',
            :lsbdistid       => operatingsystem,
            :lsbdistcodename => 'lolwut',
          }}

          it_behaves_like "all thin configurations"

          # Debian-specific examples
          it { should contain_package('thin') }
        end
      end
    end
  end
  describe "with service managed" do
    let(:params) {{
      :manage_service => true,
    }}
    context 'Debian' do
      let(:facts) {{
        :operatingsystem        => 'debian',
        :operatingsystemrelease => '7',
        :osfamily               => 'debian',
        :puppetversion          => '3.4.2',
        :concat_basedir         => '/foo',
        :kernel                 => 'linux',
        :lsbdistid              => 'debian',
        :lsbdistcodename        => 'wheezy',
      }}
      it_behaves_like "all thin configurations"

      it { should contain_package('thin') }

      it do
        should contain_service('thin').with({
          :ensure => "running"
        })
      end
    end
  end
end
