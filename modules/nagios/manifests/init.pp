class nagios {
  exec { 'apt-update':
    command => '/usr/bin/apt-get update',
  }

  package {"nagios3":
    require => Exec['apt-update'],
    ensure  => installed,
  }

  package {"nagios-nrpe-plugin":
    require => Exec['apt-update'],
    ensure  => installed,
  }

  file  { "/etc/nagios3/cgi.cfg":
    source  => "puppet:///modules/nagios/cgi.cfg",
    owner   => root,
    group   => root,
    mode    => 0644,
    notify  => Service['nagios3'],
    require => Package['nagios3','nagios-nrpe-plugin'],
  }

  file  { "/etc/nagios3/nagios.cfg":
    source  => "puppet:///modules/nagios/nagios.cfg",
    owner   => root,
    group   => root,
    mode    => 0644,
    notify  => Service['nagios3'],
    require => Package['nagios3','nagios-nrpe-plugin'],
  }

  file  { "/etc/nagios3/commands.cfg":
    source  => "puppet:///modules/nagios/commands.cfg",
    owner   => root,
    group   => root,
    mode    => 0644,
    notify  => Service['nagios3'],
    require => Package['nagios3','nagios-nrpe-plugin'],
  }

  nagios::host { 'balinux-01':
    address => "10.20.0.131",
  }

  nagios::host { 'balinux-02':
    address => "10.20.0.132",
  }

  nagios::host { 'balinux-03':
    address => "10.20.0.133",
  }

  nagios::host { 'balinux-04':
    address => "10.20.0.134",
  }

  nagios::host { 'balinux-05':
    address => "10.20.0.135",
  }

  nagios::host { 'balinux-06':
    address => "10.20.0.136",
  }

  nagios::host { 'balinux-07':
    address => "10.20.0.137",
  }

  nagios::host { 'balinux-08':
    address => "10.20.0.138",
  }

  nagios::host { 'balinux-09':
    address => "10.20.0.139",
  }

  nagios::host { 'balinux-10':
    address => "10.20.0.140",
  }
  
  $hosts = "balinux-01,balinux-02,balinux-03,balinux-04,balinux-05,balinux-06,balinux-07,balinux-08,balinux-09,balinux-10"
  
  nagios::service { 'check_load':
    host_name                   => $hosts,
    check_command               => "check_nrpe_1arg!check_load",
    service_description         => "Load",
  }

  nagios::service { 'check_users':
    host_name                   => $hosts,
    check_command               => "check_nrpe_1arg!check_users",
    service_description         => "Users",
  }

  nagios::service { 'check_zombie':
    host_name                   => $hosts,
    check_command               => "check_nrpe_1arg!check_zombie_procs",
    service_description         => "Zombie",
  }

  nagios::service { 'check_total_procs':
    host_name                   => $hosts,
    check_command               => "check_nrpe_1arg!check_total_procs",
    service_description         => "Total_procs",
  }

  nagios::service { "check_disk":
    host_name                   => $hosts,
    check_command               => "check_nrpe_1arg!check_all_disks",
    service_description         => "Disk",
  }

  nagios::service { "check_ping":
    host_name                   => $hosts,
    check_command               => "check_ping!100.0,20%!1000.0,60%",
    service_description         => "Ping",
  }

  nagios::service { "check_ssh":
    host_name                   => $hosts,
    check_command               => "check_ssh",
    service_description         => "ssh",
  }

  nagios::service { "check_http":
    host_name                   => $hosts,
    check_command               => "check_http",
    service_description         => "http",
  }

  service { "nagios3":
    require => Package["nagios3", "nagios-nrpe-plugin"],
    ensure  => running,
  }

  define nagios::host ( $address, $host_name = $title) {
    nagios_host {"${host_name}":
      require       => Package["nagios3", "nagios-nrpe-plugin"],
      notify        => Service["nagios3"],
      target        => "/etc/nagios3/conf.d/hosts.cfg",
      mode          => "0644",
      use           => "generic-host",
      check_command => "check_ping!2000.0,100%!2000.0,100%",
      host_name     => $host_name,
      address       => $address,
      alias         => $host_name,
    }
  }

  define nagios::service ($check_command, $host_name, $service_description = $title) {
    nagios_service {"${title}":
      require             => Package["nagios3", "nagios-nrpe-plugin"],
      notify              => Service["nagios3"],
      target              => "/etc/nagios3/conf.d/services.cfg",
      mode                => "0644",
      use                 => "generic-service",
      host_name           => $host_name,
      check_command       => $check_command,
      service_description => $service_description,
      check_interval      => "1",
    }
  }
}

