#!/bin/bash
dpkg -s collectd
if [ $? -eq 0 ] ; then exit; fi

apt-get update
apt-get install -y checkinstall
tar jxf /tmp/collectd_5.5.0.tar.bz2 &&
cd collectd-5.5.0/ &&
./configure &&
checkinstall -y
