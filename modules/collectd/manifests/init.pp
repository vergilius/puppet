class collectd {

	file { "/tmp/collectd_5.5.0.tar.bz2":
    owner   => root,
    group   => root,
    mode    => 0644,
    ensure  => present,
    source 	=> "puppet:///modules/collectd/collectd-5.5.0.tar.bz2",
  }

  file { "/tmp/install_collectd.sh":
    owner   => root,
    group   => root,
    mode    => 0665,
    ensure  => present,
    source 	=> "puppet:///modules/collectd/install_collectd.sh",
  }

	exec {"install_collectd":
		require 	=> File["/tmp/install_collectd.sh"],
  	command   => "/tmp/install_collectd.sh",
  }

  #package {"collectd":
	#  ensure  	=> latest,
	#  provider 	=> dpkg,
	#  source 		=> "/opt/collectd_5.5.0-1_amd64.deb",
  #}

  file { "/opt/collectd/etc/collectd.conf":
    require => Exec["install_collectd"],
  	ensure	=> present,
    owner   => root,
    group   => root,
    mode    => 0644,
    source 	=> "puppet:///modules/collectd/collectd.conf",
  }

  exec {"start_collectd":
  	command   => "/opt/collectd/sbin/collectd",
  	subscribe	=> File["/opt/collectd/etc/collectd.conf"],
  }
}