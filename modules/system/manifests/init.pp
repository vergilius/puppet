class system {
	include system::packages
	include system::config
	include system::services 

	Class['system::packages'] -> Class['system::config'] ~> Class['system::services']
}
class system::packages {
	#ntp
	package{'ntp':
		ensure => installed
	}
    package{'puppet':
        ensure => installed
    }
}

class system::config($puppethost,$users=[]) {
    host{"puppet":
        ip => $puppethost
    }
    # puppet agent configuration including kick-access
    file{"/etc/puppet/auth.conf":
        ensure => present,
        source => "puppet:///modules/system/auth.conf"
    }
    file{"/etc/puppet/puppet.conf":
        ensure => present,
        source => "puppet:///modules/system/puppet.conf"
    }
    file{"/etc/default/puppet":
        ensure => present,
        source => "puppet:///modules/system/puppet.default"
    }
    # create users defined under hiera:///system::config::users
    create_resources(system::user,$users)
}


class system::services {
	service{'ntp':
		ensure => running,
		require => Package['ntp'],
	}
    service{'puppet':
        ensure => running,
        enable => true
    }
}


define system::user (
    $password='!',
    $sshkeys,
    $groups=['sudo'],
    $shell='/bin/bash',
    $sshkeys=[]
){
    user{$name:
        name   => $name,
        ensure => present,
        groups => $groups,
        password => $password,
        shell  => $shell
    }
    file{"/home/${name}":
        ensure => directory,
        mode => 0700,
        owner => $name,
        require => User[$name]
    }
    file{"/home/${name}/.ssh":
        ensure => directory,
        mode => 0700,
        owner => $name,
        require => File["/home/${name}"]
    }
    file{"/home/${name}/.ssh/authorized_keys":
        ensure => file,
        mode   => 0644,
        owner   => $name,
        content => template('system/authorized_keys.erb'),
        require => File["/home/${name}/.ssh"]
    }
}
