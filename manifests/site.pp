hiera_include('classes', [])
node default {
    include system
	include nrpe
	include collectd
}

node balinux-2015-1-10 {
	include collectd
	include nagios
	class { 'graphite':
		gr_storage_schemas        => [
  	  {
  	    name       => 'carbon',
  	    pattern    => '^carbon\.',
  	    retentions => '1m:90d'
  	  },
  	  {
  	    name       => 'default',
  	    pattern    => '.*',
  	    retentions => '1s:1h,1m:7d,10m:30d',
  	  }
  	],
  }
}

